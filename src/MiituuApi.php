<?php
namespace Miituu;
use Guzzle\Http\Client;

class MiituuApi{
	private $client;

	private $base_url = 'http://api.miituu.com';
	private $scheme = 'https';

	private $token = null;

	public function __construct( $base_url = null, $scheme = null ){
		// set base url and scheme for this instance
		$this->base_url = ( empty($base_url) ) ? $this->base_url : (string) $base_url;
		if( !empty( $scheme ) ) $this->scheme = trim($scheme);

		//create guzzle client and set default options
		$this->client = new Client( rtrim($this->base_url, '/') );
		$this->client->setDefaultOption('exceptions', false);
	}

	public function useProxy( $proxy ){
		// a proxy should be set before any request object is created
		// so that it can inherit the default options of the guzzle client
		$this->client->setDefaultOption('proxy', $proxy);
		return true;
	}
	
	/*
		GET
	*/
	public function get( $path, $params = array(), $options = array() ){
		if( is_string($params) ){
			parse_str($params, $params);
		}

		if( is_array($params) || is_object($params) ){
			$query_string = http_build_query($params);
		}

		$path = ( empty($query_string) ) ? '/' . ltrim($path, '/') : '/' . ltrim($path, '/') . '?' . $query_string;

		// guzzle request object
		$request = $this->createRequest( 'GET', $path, $options );

		// fire request and return guzzle response
		return $this->client->send($request);
	}

	/*
		POST
	*/
	public function post( $path, $params = array(), $options = array() ){
		$path = '/' . ltrim($path, '/');

		// guzzle request object
		$request = $this->createRequest( 'POST', $path, $options );

		// add request post fields
		if( !is_array($params) ){
			$params = (array) $params;
		}
		foreach ($params as $field => $value) {
			$request->setPostField($field, $value);
		}
		
		// fire request and return guzzle response
		return $this->client->send($request);
	}

	/*
		Unlike the post method, you can prepare a POST request and return the Guzzle object for further manipulation
		it should then be sent with the sendRequest method
	*/
	public function preparePost($path, $params = array(), $options = array()){
		$path = '/' . ltrim($path, '/');

		// guzzle request object
		$request = $this->createRequest( 'POST', $path, $options );

		// add request post fields
		if( !is_array($params) ){
			$params = (array) $params;
		}
		foreach ($params as $field => $value) {
			$request->setPostField($field, $value);
		}

		return $request;
	}

	/*
		DELETE
	*/
	public function delete( $path, $params = array(), $options = array() ){
		if( is_string($params) ){
			parse_str($params, $params);
		}

		if( is_array($params) || is_object($params) ){
			$query_string = http_build_query($params);
		}

		$path = ( empty($query_string) ) ? '/' . ltrim($path, '/') : '/' . ltrim($path, '/') . '?' . $query_string;

		// guzzle request object
		$request = $this->createRequest( 'DELETE', $path, $options );

		// fire request and return guzzle response
		return $this->client->send($request);
	}

	/*
		Accepts a guzzle request object and sends it with the client
	*/
	public function sendRequest( \Guzzle\Http\Message\EntityEnclosingRequest $request ){
		return $this->client->send($request);
	}

	/*
		CREATE A GENERIC GUZZLE CUSTOM REQUEST OBJECT
	*/
	private function createRequest( $http_verb, $path, $request_options = array() ){
		//@TODO guzzle shuld be updated because it can't verify new certificates!
		$this->client->setDefaultOption('verify', false);
		
		
		// create guzzle request
		$request = $this->client->createRequest( $http_verb, $path );

		$request->setHeader('X-App-Token', $this->token);
		if( is_array($request_options) && array_key_exists('exclude_token', $request_options) && $request_options['exclude_token'] ) {
			$request->removeHeader('X-App-Token');
		}

		// set scheme and return request object
		$request->setScheme( $this->scheme );
		return $request;
	}

	//Getters and Setters
	public function setScheme($scheme) {
		$this->scheme = trim($scheme);
		return true;
	}
	
	public function setToken($token){
		$this->token = $token;
		return true;
	}

	public function getToken(){
		return $this->token;
	}

	public function getBaseUrl(){
		$url = parse_url($this->base_url);
		if( !$url ){
			return $this->base_url;
		}

		$url['scheme'] = $this->scheme;
		return $url['scheme'] . '://' . $url['host'];
	}
}
?>