<?php
namespace Miituu;

class ApiConfig {
	// Token Levels
    const LEVEL_PUBLIC            = 10;
    const LEVEL_TRUSTED           = 20;
    const LEVEL_ADMIN             = 30;
    const LEVEL_OWNER             = 40;
    const LEVEL_MIITUU            = 50;

    // New status constants
    const STATUS_PRIVATE    = 10;
    const STATUS_PROTECTED  = 20;
    const STATUS_PUBLIC     = 30;

    const STATUS_FAILED     = 60;
    const STATUS_PROMISED   = 70;
    const STATUS_PENDING    = 80;
    const STATUS_DELETED    = 90;

    // Active/inactive are used for administrators etc
    const STATUS_INACTIVE   = 10;
    const STATUS_ACTIVE     = 30;

    // Used for uploads
    const STATUS_UPLOADED   = 80;
    const STATUS_COMPLETE   = 20;

    // Used for exports
    const STATUS_QUEUED     = 70;
    //const STATUS_UNPUBLISHED      = 0;
}
?>